* ներածություն
* * OS and State
* * ֆոն֊նեյմանի ճարտարապետություն
* * cpu supervisor mode vs. user mode, CPU modes, addressing mechanisms
* * հարվարդի ճարտարապետություն
* * linux vs bsd vs lfs
* * microkernel, message passing, monolith, oberon/plan9/inferno
* * kernel modules

* basics
* * vt100
* * stdin/stdout/stderr, fds, "everything is a file", /dev/*
* * syscall, exit(), ret(asm), write()/read(), strace(stderr)
* * difference between glibc vs. kernel calls, two usenet 1994 links
* * exercise: spellcheck, ./lowercase, compare
* * exercise: cat /dev/input/mice
* * exercise: read man and man ps

* պրոցես
* * պրոցեսի կառուցվածքը հիշողության մեջ
* * * stack, heap, code, bss, data, anonym memory, stack overflow/gets/canary protection/stack smashing examples
* * pid, fork(), exec(), wait(), bg/fg processes, top/htop, kill, example of shell
* * argc, argv, envp
* * ctrl-z, process states, D state, process scheduling, context switch(vmstat)
* * zombie processes
* * cgroups, namespaces, process isolation, docker, docker diy
* * exercise: man 3 getopt, getopt, getenv
* * exercise: shell with bg support

* fs
* * chmod, chown, getsuid

* binaries
* * types of binaries
* * object files
* * linking - dyanamic, static, dl
* * makefile, cmake

* mm
* * malloc(), free(), calloc(), realloc(), char strings
* * allocation strategies
* * segmentation, segfaults
* * mmap()(bank example), overcommit
* * paging, getpagesize, swap, tlb, virtual -> physical, page directories
* * virtual address space randomization
* * exercise: interpret top(res/virt/shr)

* IPC
* * pipes(also named), signals
* * semaphores, shared memory
* * message passing, message queues (POSIX)
* * dbus, unix domain sockets
* * exercise: shell with support of pipeing

* networking
* * Twisted pair, switch hub
* * OSI layers, TCP/ethernet payloads, duplex/full/half, NATO example
* * IPv4, IPv6, MAC(with randomization), DNS
* * broadcast/unicast sockets
* * HTTP/HTTPS/SSL/TLS
* * bsd sockets, ioctl
* * async, poll/select, epoll, libuv, why nodejs works?
* * serialization
* * SOAP, REST, GraphQL, gRPC
* * kubernetes networking model, kubernetes diy
* * exercise: be jee networking guide
* * exercise: forking server

* threads
* * monitors, dining philosophers, sleeping burber
* * locking strategies, mutex, condition variable
* * why its not faster twice with two CPUs?
* * thread safe ds
* * atomic, x86/java/go memory models
* * concurrency, green thread, goroutines, how go runtime works?, project loom
* * structured concurrency?
* * lock free, wait free
* * exercie: thread pool + server

* distributed systems
